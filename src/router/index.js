import { createRouter, createWebHistory } from "vue-router";
// import HomePage from "../views/HomePage.vue"
// import AboutPage from "../views/AboutPage.vue";
import NotFound from "@/views/NotFound.vue";

const routes = [
    {
        path: '/',
        name:'home',
        component: ()=> import('@/views/HomePage.vue'),
    },
    {
        path: '/about',
        name:'about',
        component: ()=> import('@/views/AboutPage.vue'),
    },
    {
        path: '/user',
        name:'user',
        component: ()=> import('@/views/UsersPage.vue'),
    },
    {
        path: '/user/:id',
        name:'UserSingle',
        component: ()=> import('@/views/UserSinglePage.vue'),
        props:true
    },
    {
        path:'/member',
        redirect:'/about'
    },
    {
        path:'/:pathMatch(.*)*',
        name:'NotFound',
        component:NotFound,
    }
]

const router = createRouter({
    history:createWebHistory(),
    routes,
})

export default router